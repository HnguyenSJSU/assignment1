package com.company;

/* Hung Nguyen 013626210 */
public class E1_11 {
    public static void main(String[] args) {
        System.out.println(
                "    - - - - . - . - - . - . -.         [_____] \n" +
                        "   /                  ( )  o \\      _____| |_____ \n" +
                        "  /|                       | |      |   hello   |\n" +
                        " / |  |\\  \\ --- |  |\\__\\   uwu      |  everyone | \n  " +

                        ".|__| \\__\\    |__| \\__\\           |___________| \n"
        );
    }
}
/*
output:
"D:\java compiler\bin\java.exe" "-javaagent:D:\intellij\IntelliJ IDEA Community Edition 2021.2\lib\idea_rt.jar=51450:D:\intellij\IntelliJ IDEA Community Edition 2021.2\bin" -Dfile.encoding=UTF-8 -classpath "D:\cs49j\Assignment1\out\production\Assignment1;D:\intellij\IntelliJ IDEA Community Edition 2021.2\plugins\Kotlin\kotlinc\lib\kotlin-stdlib.jar;D:\intellij\IntelliJ IDEA Community Edition 2021.2\plugins\Kotlin\kotlinc\lib\kotlin-reflect.jar;D:\intellij\IntelliJ IDEA Community Edition 2021.2\plugins\Kotlin\kotlinc\lib\kotlin-test.jar" E1_11
    - - - - . - . - - . - . -.         [_____]
   /                  ( )  o \      _____| |_____
  /|                       | |      |   hello   |
 / |  |\  \ --- |  |\__\   uwu      |  everyone |
  .|__| \__\    |__| \__\           |___________|


Process finished with exit code 0
 */