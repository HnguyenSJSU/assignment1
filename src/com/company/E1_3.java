package com.company;

/* Hung Nguyen 013626210 */
public class E1_3 {
    public static void main(String[] args) {
        Integer nTotal = 1;
        for(int nIndex = 1; nIndex <= 10; nIndex++) {
            nTotal *= nIndex;
        }
        String strDisplay = String.format("The product is the first ten positive integers, 1x2x..x10: %s", nTotal.toString());
        System.out.println(strDisplay);

    }
}
/*
"D:\java compiler\bin\java.exe" "-javaagent:D:\intellij\IntelliJ IDEA Community Edition 2021.2\lib\idea_rt.jar=51455:D:\intellij\IntelliJ IDEA Community Edition 2021.2\bin" -Dfile.encoding=UTF-8 -classpath "D:\cs49j\Assignment1\out\production\Assignment1;D:\intellij\IntelliJ IDEA Community Edition 2021.2\plugins\Kotlin\kotlinc\lib\kotlin-stdlib.jar;D:\intellij\IntelliJ IDEA Community Edition 2021.2\plugins\Kotlin\kotlinc\lib\kotlin-reflect.jar;D:\intellij\IntelliJ IDEA Community Edition 2021.2\plugins\Kotlin\kotlinc\lib\kotlin-test.jar" E1_3
The product is the first ten positive integers, 1x2x..x10: 3628800

Process finished with exit code 0
 */